#/bin/sh

# Build components
MAKE="cmake"
MAKEFILE_TYPE="Unix Makefiles"
TYPE="Debug"
OS="Linux"
UNIX_ARCH="amd64"
MSDOS_ARCH="x86_64"
VERSION="7.0"
VERSION_REF=$(git rev-parse --abbrev-ref HEAD)
TAR="tar -czf"
ZIP="zip -9r"
QT_VERSION=5
PROC=1

# Check variables
NEED_HELP=0
CHOOSEN_OS=0
CHOOSEN_TYPE=0
CHOOSEN_QT=0
VERBOSE=""
COMPRESS=0

# Colours
RED="\033[1;31m"
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
BLUE="\033[1;34m"
PURPLE="\033[1;35m"
CYAN="\033[1;36m"
RESET_COLOUR="\033[0m"

# Read options
OPTIONS=`getopt -o o:t:q:p:rvh --long operating-system:,build-type:,qt-version:,processing-units:,release,verbose,help -- "$@"`
eval set -- "$OPTIONS"

# Handle options
while true
  do case "$1" in
    -o|--operating-system)
      OS="$2"
      CHOOSEN_OS=1
      shift 2
      ;;
    -t|--build-type)
      TYPE="$2"
      CHOOSEN_TYPE=1
      shift 2
      ;;
    -q|--qt-version)
      QT="$2"
      CHOOSEN_QT=1
      shift 2
      ;;
    -p|--processing-units)
      PROC="$2"
      shift 2
      ;;
    -r|--release)
      COMPRESS=1
      shift
      ;;
    -v|--verbose)
      VERBOSE="--verbose"
      shift
      ;;
    -h|--help)
      NEED_HELP=1
      shift
      ;;
    --) shift; break ;;
    *) break ;;
  esac
done

# void: Print help
help_function()
{
  printf "ShadyLauncher automation tool v0.1\n"
  printf "\n"
  printf "Usage:\n"
  printf "  -o|--operating-system   -   Name of the system/kernel (Linux/Windows).\n"
  printf "  -t|--build-type         -   Release option (Debug/Release).\n"
  printf "  -q|--qt-version         -   Qt version (5/6)\n"
  printf "  -p|--processing-units   -   Number of cores. Available cores: $(nproc).\n"
  printf "  -r|--release            -   Package the launcher in compressed format.\n"
  printf "  -v|--verbose            -   LOG EVERYTHING.\n"
  printf "\n"
  printf "Help:\n"
  printf "  -h|--help               -   Print me.\n"
}

# void: Template for automated TARGET building
build_target()
{
  BUILD_FOLDER="ShadyLauncher-${VERSION}-${VERSION_REF}-${OS}-${UNIX_ARCH}-${TYPE}-Qt${QT_VERSION}"
  case $COMPRESS in
    1) RELEASE_FOLDER="i_ShadyLauncher-${VERSION}-${VERSION_REF}-${OS}-${UNIX_ARCH}-${TYPE}-Qt${QT_VERSION}"; ;;
    *) RELEASE_FOLDER="ShadyLauncher"; ;;
  esac
  RELEASE_FILE="ShadyLauncher-${VERSION}-${VERSION_REF}-${OS}-${UNIX_ARCH}-${TYPE}-Qt${QT_VERSION}.tar.gz"

  printf "${RED}>>>${RESET_COLOUR} Preparing build files for ${GREEN}${OS} ${TYPE}${RESET_COLOUR} with the design of ${PURPLE}Qt${QT_VERSION}${RESET_COLOUR}.\n"
  ${MAKE} -S .. -B ${BUILD_FOLDER} \
                -DCMAKE_BUILD_TYPE=${TYPE} \
                -DCMAKE_INSTALL_PREFIX=${RELEASE_FOLDER} \
                -DLauncher_QT_VERSION_MAJOR=${QT_VERSION} \
                -DENABLE_LTO=ON \
                -G "${MAKEFILE_TYPE}"

  printf "${RED}>>>${RESET_COLOUR} Building for ${GREEN}${OS} ${TYPE}${RESET_COLOUR} with the design of ${PURPLE}Qt${QT_VERSION}${RESET_COLOUR}.\n"
  ${MAKE} --build ${BUILD_FOLDER} -j${PROC}

  printf "${RED}>>>${RESET_COLOUR} Packing release.\n"
  ${MAKE} --install ${BUILD_FOLDER}
  ${MAKE} --install ${BUILD_FOLDER} --component portable

  case $COMPRESS in
    1)
      mv ${VERBOSE} ${RELEASE_FOLDER} ShadyLauncher-${VERSION}-${VERSION_REF}
      if [ "${OS}" == "Linux" ]
        then ${TAR} ${RELEASE_FILE} ShadyLauncher-${VERSION}-${VERSION_REF} ${VERBOSE}
        else ${ZIP} ${RELEASE_FILE} ShadyLauncher-${VERSION}-${VERSION_REF} ${VERBOSE}
      fi
      mv ${VERBOSE} ShadyLauncher-${VERSION}-${VERSION_REF} ${RELEASE_FOLDER}
      rm ${VERBOSE} ShadyLauncher-${VERSION}-${VERSION_REF} 2&>.error_cache
      rm ${VERBOSE} .error_cache
      mkdir -p ${VERBOSE} release
      mv ${VERBOSE} ${RELEASE_FILE} release
      ;;
    *) ;;
  esac

  printf "${RED}>>>${RESET_COLOUR} ${GREEN}${OS} ${TYPE}${RESET_COLOUR} ${PURPLE}Qt${QT_VERSION}${RESET_COLOUR} ready for launch.\n"
}

case $NEED_HELP in
  1)
    help_function
    exit 0
    ;;
  *) ;;
esac

case $CHOOSEN_OS in
  0)
    printf "Input your OS (Linux/Windows): "
    read OS
    ;;
  *) ;;
esac

case $CHOOSEN_TYPE in
  0)
    printf "Input your TYPE (Debug/Release): "
    read TYPE
    ;;
  *) ;;
esac

case $CHOOSEN_QT in
  0)
    printf "Input your QT (5/6): "
    read QT
    ;;
  *) ;;
esac

# Compile and release
build_target
